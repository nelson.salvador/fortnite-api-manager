# Fortnite Manager

A library to interact with Fortnite services

## Installation
```
npm i fortnite-api-manager
```

## How to Use?
Example: 
```javascript
const Client = require('fortnite-api-manager');

(async () => {

let accountId = 'Your Account Id'
let deviceId = 'Your Device Id'
let secret = 'Your Secret'

let config = {
  'accountId':accountId,
  'deviceId':deviceId,
  'secret':secret
};

let client = new Client(config); // Create a Client

await client.login(); // Authenticate Your Credentials

let exchangeCode = await client.exchange(); // Generate an Exchange Code

console.log(exchangeCode) // Gives Your Exchange Code in Console Log

await client.logout()
})();
```

# Documentation

## Client
```javascript
const Client = require('fortnite-api-manager')
const client = new Client(config)
```

### Client Properties
- accountId
- deviceId
- secret
- isLogin
- token

### Client Methods
- login()
- logout()
- getRequest(url)
- postRequest(url , body)
- exchange()
- getToken()
- getAccount(accountId)
- getAccountByName(epicName)
- campaignProfile(accountId)
- brProfile()

# Help
Feel free to join [our Discord server](https://fishstickbot.com/discord) to give Suggestions or for Help.

# License
MIT License

Copyright (c) 2020-2021 Vanxh.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
