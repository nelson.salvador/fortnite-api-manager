/**
 * fortnite-manager
 * Module to make API calls to fortnite
 *
 * @author Vanxh
 */

// modules
const axios = require('axios');

// internal functions
const {generateToken} = require('./functions')

// fortnite-services
const accountServices = `https://account-public-service-prod.ol.epicgames.com/account/`
const fortniteServices = `https://fortnite-public-service-prod11.ol.epicgames.com/fortnite/api/game/v2/profile/`

// main class
class Client {

    /**
     * constructor()
     *
     * @param {object} [config]
     * @param {string} [config.accountId]
     * @param {string} [config.deviceId]
     * @param {string} [config.secret]
     */
    constructor(config) { // TO-DO Support More Login Methods
      
        if (!config) {
            throw new Error("Provide Client Configuration.");
        }
        
        this.accountId = config.accountId
        if (!this.accountId) throw new Error("No accountId Provided")
        
        this.deviceId = config.deviceId
        if (!this.deviceId) throw new Error("No deviceId Provided")
        
        this.secret = config.secret
        if (!this.secret) throw new Error("No secret Provided")
        
        this.token = false
        this.isLogin = false
  }
  
    /**
     * login()
     * Login your client
     *
     * @returns Success/Error
     */
     async login(){
      let token = await generateToken(this.accountId , this.deviceId , this.secret )
      
      if (!token) throw new Error("Invalid Credential\'s Provided")
      
      this.token = token
      this.isLogin = true
      console.log('Client Successfully Connected to Fortnite')
      
      return token
     }
     
    /**
     * logout()
     * Logout your client
     *
     * @returns Success/Error
     */
     async logout(){
      if (!this.login) throw new Error("You are not Logged-In")
      
      // to do delete session
      
      this.token = false
      this.isLogin = false
      
      console.log('Client Successfully Disconnected from Fortnite')
      
      return true
     }
  
    /**
     * getRequest()
     * Use axios to query API
     *
     * @param {string} URL
     * @returns {Promise<object>}
     */
     async getRequest(URL) {
       try{
         var config = {
           method : 'get',
          url : URL,
          headers: {
            'Authorization': 'bearer ' + this.token
          }
         }
         let response = (await axios(config)).data
         return response
       }catch(e){
         return e
       }
     }
     
    /**
     * postRequest()
     * Use axios to send API Request
     *
     * @param {string} URL
     * @param {object} body
     * @returns {Promise<object>}
     */
     async postRequest(URL , body) {
       try{
         var config = {
           method : 'post',
          url : URL,
          headers: {
            'Authorization': 'bearer ' + this.token,
            'Content-Type': 'application/json'
          },
          data : JSON.stringify(body)
         }
         let response = (await axios(config)).data
         return response
       }catch(e){
         console.log(e)
         return e
       }
     }
  
    /**
     * exchange()
     * Generates an Exchange Code
     *
     * @returns {Promise<string>}
     */
     async exchange() {
       if (!this.login) throw new Error("You are not Logged-In")
       const url  = `${accountServices}api/oauth/exchange`
       let response = await this.getRequest(url)
       try{
         return response['code']
       }catch{
         throw new Error(response)
       }
     }
     
    /**
     * getToken()
     * Returns your Authenticated Access Token
     *
     * @returns token
     */
     getToken() {
       if (!this.login) throw new Error("You are not Logged-In")
       return this.token
     }
     
    /**
     * getAccount(id)
     * Returns an Account by Id
     *
     * @returns {Promise<object>}
     */
     async getAccount(id){
       if (!this.login) throw new Error("You are not Logged-In")
       let accountId = id || this.accountId
       const url = `${accountServices}api/public/account/${accountId}`
       let accountData = await this.getRequest(url)
       if (!accountData) return `AccountId ${accountId} was not Found`
       return accountData
     }
     
    /**
     * getAccountByName(epicname)
     * Returns an Account by Epic Name
     *
     * @returns {Promise<object>}
     */
     async getAccountByName(epicname){
       if (!this.login) throw new Error("You are not Logged-In")
       if (!epicname) throw new Error("No Epic Name Provided")
       const url = `${accountServices}api/public/account/displayName/${encodeURI(epicname)}`
       let accountData = await this.getRequest(url)
       if (!accountData) return `${epicname} not Found`
       return accountData
     }
     
    /**
     * campaignProfile(accountId)
     * Returns Campaign Profile from Account Id of anyone
     *
     * @returns {Promise<object>}
     */
     async campaignProfile(id){
       if (!this.login) throw new Error("You are not Logged-In")
       let accountId = id || this.accountId
       const url = `${fortniteServices}${accountId}/public/QueryPublicProfile?profileId=campaign`
       let stwData = await this.postRequest(url , {})
       return stwData
     }
     
    /**
     * brProfile()
     * Returns your Battle Royale Profile
     *
     * @returns {Promise<object>}
     */
     async brProfile(){
       if (!this.login) throw new Error("You are not Logged-In")
       let accountId = this.accountId
       const url = `${fortniteServices}${accountId}/client/QueryProfile?profileId=athena`
       let brData = await this.postRequest(url , {})
       if (!brData) return false
       return brData
     }

}

module.exports = Client